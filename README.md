Here's a simple drag-to-rotate globe demo. 

When trying to write an interactive globe viz, I was surprised to find that there were no existing function / tutorial / example code out there to accomplish the simple drag-to-rotate functionality. [Jason Davies][1]  has a good article on the theory behind this, but no code. It is still difficult for people with limited math background (like me) on matrices / quaternion / euler angles to figure it out. 

One big hurdle is to understand why and how to convert between different coordinate / representations for the rotations. e.g. Quaternion is easy to combine together by multiplication, that's why it's chosen here. and d3 uses Euler angle representations for projection rotations, that's why we need to convert between them. 

Most of the formulas are from the [wiki][2] with minor tweaks. 

[1]: https://www.jasondavies.com/maps/rotate/
[2]: https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions


forked from <a href='http://bl.ocks.org/ivyywang/'>ivyywang</a>'s block: <a href='http://bl.ocks.org/ivyywang/7c94cb5a3accd9913263'>Drag to Rotate the Globe</a>